program merge;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, Generics.Collections;

var
  list, ff, failf: TextFile;
  data: TDictionary<Integer, string>;
  alist: TList<Integer>;
  s, s1, fn: string;
  prev, v, n: integer;
begin
  try
    data := TDictionary<Integer, string>.Create;
    alist := TList<Integer>.Create;
    AssignFile(list, ParamStr(1));
    Reset(list);
    repeat
      Readln(list, fn);
      AssignFile(ff, fn);
      if not FileExists(fn) then
      begin
        Writeln('!!!not found: '+fn);
        continue;
      end;
      Reset(ff);
      repeat
        Readln(ff, s);
        n := Pos(':', s);
        if n < 0 then
          continue;
        s1 := Copy(s, 1, n-1);
        Delete(s, 1, n+1);
        v := abs(StrToInt(s1));
        if data.ContainsKey(v) then
          //data[v] := data[v] + ', '+s
        else
        begin
          data.Add(v, s);
          alist.Add(v);
        end;
      until Eof(ff);
      CloseFile(ff);
    until Eof(list);
    CloseFile(list);

    AssignFile(ff, ParamStr(2));
    Rewrite(ff);
    AssignFile(failf, ParamStr(3));
    Rewrite(failf);
    alist.Sort;
    prev := -1;
    for v in alist do
    begin
      writeln(ff, v, ': ',data[v]);
      if v - prev > 1 then
        writeln(failf, '[', v - prev,']:', prev, '-', v);
      prev := v;
    end;
    CloseFile(ff);
    CloseFile(failf);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
