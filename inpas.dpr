//10958
program inpas;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils, Generics.Collections, Math, StrUtils;

const
  MAX_N = 8;
  WRONG_VAL = Infinity;
  WRONG_INT = MaxInt;
  EPSILON = 1e-9;

type
  TOperation = (Concat, Plus, Minus, Multiply, Divide, Power, PowerNeg, NO_OP);
  TOrder = array [1 .. MAX_N] of Integer;
  TOperations = array [1 .. MAX_N] of TOperation;

  TNumber = Extended;

  TFoundItem = record
    no: Integer;
    operations: TOperations;
    order: TOrder;
    function ToString: string;
  end;

  TFound = TDictionary<Integer, TFoundItem>;

  TElement = record
    Prev, Next: Integer;
    op: TOperation;
    Value: TNumber;
    constructor Create(aindex: Integer; aval: TNumber; aop: TOperation);
    constructor CreateLast(aindex: integer; aval: TNumber);
    function ToString: String;
  end;

  TExpression = record
    n: integer;
    data: array [1 .. MAX_N * 2 + 1] of TElement;
    function Evaluate(const order: TOrder): Integer;
    constructor Create(ops: TOperations);
    function Dump: string;
  end;

const OP_CHAR: array[TOperation] of string = ('','+','-','*','/','^','^-','?');

var
  TOTAL: integer = 0;
  { TExpression }

constructor TExpression.Create(ops: TOperations);
var
  I, pos: Integer;
  v: TNumber;
begin
//  N := 1;
  pos := 1;
  v := 0;
  for I := 1 to MAX_N+1 do
  begin
    v := v*10 + I;
    if I > MAX_N then
      break;
    if ops[I] = Concat then
      continue;
    data[pos] := TElement.Create(pos, v, NO_OP);
    Inc(pos);
    data[pos] := TElement.Create(pos, 0, ops[I]);
    Inc(pos);
    v := 0;
  end;
  data[pos] := TElement.CreateLast(pos, v);
  inc(pos);
  N := (pos div 2)-1;
end;

function DoOp(op: TOperation; v1, v2: TNumber): TNumber;
var
  x: TNumber;
begin
  try
    Result := WRONG_VAL;
    case op of
      Plus: Result := v1+v2;
      Minus: Result := v1-v2;
      Multiply: Result := v1*v2;
      Divide:
        begin
          if v2 = 0 then exit;
          Result := v1 / v2;
        end;
      Power, PowerNeg:
      begin
//        if abs(v2) >= 64 then exit;
        if op = PowerNeg then
          v2 := -v2;
        if v1 = 0 then
        begin
          Result := 0;
          exit;
        end;
        x := Math.Power(abs(v1), v2);
        Result := x
      end
    end;
  except
    on E: EMathError do
      Result := WRONG_VAL;
  end;
end;

function TExpression.Dump: string;
var
  it: integer;
begin
  Result := '';
  it := 1;
  repeat
    Result := Result + Data[it].ToString;
    it := data[it].Next;
  until it <= 0;
end;

function TExpression.Evaluate(const order: TOrder): Integer;
var
  node, I, n1, n2: Integer;
  v: TNumber;
begin
  inc(TOTAL);
  Result := WRONG_INT;
  for I := 1 to N do
  begin
    node := order[I]*2;
    n1 := data[node].Prev;
    n2 := data[node].Next;
    v := DoOp(data[node].op, data[n1].Value, data[n2].Value);
    if v = WRONG_VAL then
      exit;
    data[n1].Value := v;
    if data[n2].Next > 0 then
    begin
      data[data[n2].Next].Prev := n1;
    end;
    data[n1].Next := data[n2].Next;
//    Writeln(Dump)
  end;
  if Abs(Frac(data[1].Value)) > EPSILON then exit;
  if abs(data[1].Value) > MaxInt-1 then exit;
  Result := abs(Trunc(data[1].Value))
end;

procedure EvaluateAll(ops: TOperations; found: TFound);
var
  res, I, z, y, l, v, j: Integer;
  p: TOrder;
  base, copy: TExpression;
  report: TFoundItem;
begin
  base := TExpression.Create(ops);
  for I := 1 to base.N do
  begin
    p[I] := base.N + 1 - I;
  end;
  z := 1;
  for I := 1 to base.N do
    z := z * I;
  for y := 2 to z + 1 do
  begin
    copy := base;
    res := copy.Evaluate(p);
    if not found.ContainsKey(res) then
    begin
      report.operations := ops;
      report.order := p;
      report.no := base.n;
      found.Add(res, report);
    end;
    if y > z then
      break;
    I := 2;
    j := 1;
    while p[I - 1] <= p[I] do
      I := I + 1;
    while p[j] <= p[I] do
      j := j + 1;
    v := p[I];
    p[I] := p[j];
    p[j] := v;
    if I <> 2 then
    begin
      for l := 1 to (I - 1) div 2 do
      begin
        v := p[l];
        p[l] := p[I - l];
        p[I - l] := v
      end;
    end;
  end
end;

{ TElement }

constructor TElement.Create(aindex: Integer; aval: TNumber; aop: TOperation);
begin
  Prev := aindex - 1;
  Next := aindex + 1;
  Value := aval;
  op := aop;
end;


constructor TElement.CreateLast(aindex: integer; aval: TNumber);
begin
  Prev := aindex-1;
  Next := 0;
  Value := aval;
  op := NO_OP;
end;

function TElement.ToString: String;
begin
  if op = NO_OP then
    result := FloatToStr(Value)
  else
    Result := OP_CHAR[op]
end;

{ TFoundItem }

function TFoundItem.ToString: string;
var
  i: integer;
begin
  Result := '';
  for I := 1 to MAX_N do
    Result := Result+IntToStr(I)+OP_CHAR[operations[i]];
  Result := Result+IntToStr(MAX_N+1)+' (';
  for I := 1 to no do
    Result := Result+IntToStr(order[I])+', ';
  SetLength(Result, length(Result)-2);
  Result := Result+')';
end;


procedure ProcessNumber(n: Integer; found: TFound);
var
  aops: TOperations;
  i: integer;
begin
  for I := 1 to MAX_N do
  begin
    aops[I] := TOperation((n mod 7));
    n := n div 7;
  end;
  EvaluateAll(aops, found);
end;

var
  Found: TFound;
  i: integer;
  t: TDateTime;
  te: TextFile;
  baseperc, perc: integer;
begin
  t := Now;
  try
    Found := TFound.Create;
    writeln(Stringofchar('.',100));
    baseperc := (StrToInt(ParamStr(2)) - StrToInt(ParamStr(1))) div 100;
    perc := baseperc;
    for I := StrToInt(ParamStr(1)) to StrToInt(ParamStr(2)) do
    begin
      ProcessNumber(I, found);
      dec(perc);
      if perc <= 0 then
      begin
        write('+');
        perc := baseperc;
      end;
    end;
    AssignFile(te, ParamStr(3));
    Rewrite(te);
    for I in Found.Keys do
      if I <> MaxInt then
        writeln(te, I, ': ',Found[i].ToString);
    CloseFile(te);
  writeln('');
  Writeln('---evaluations: '+IntToStr(TOTAL));
  Writeln('---passed: '+TimeToStr(Now - t));
  readln;
  except
    on E: Exception do
    begin
      Writeln(E.ClassName, ': ', E.Message);
      readln;
    end;
  end;
end.
